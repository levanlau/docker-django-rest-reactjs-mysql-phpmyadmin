# README #

This tutorial uses docker(+ django, + mysql, +phpmyadmin) tools to build an app which includes django in role "backend", react app in role "frontend", "backend"
works with "frontend" throught api "RESTful"

# Step 1: building backend

- docker-compose run web django-admin.py startproject myproject .

- cd backend/myproject > nano settings , add code below to DATABASES = { ... }

  'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'django20',
        'USER': 'root',
		'PASSWORD': 'mypass',
        'HOST': 'db',
        'PORT': 3306,
    }
	
- docker-compose up -d (run background) or: docker-compose up (run foreground to view and bug error)

# Step 2: view backend url

- View http://localhost:8000 (django)

- View http://localhost:8082 (phpmyadmin)

# Step 3: configurate more backend

- docker exec -it c_web_django_rest bash ("c_web_django_rest" is container name)

- Inside the bash command, type: "python3.5 manage.py migrate" to create database for django, then type: "python3.5 manage.py createsuperuser" to create admin user, the last type: "exit" to exit command window.

- View http://localhost:8000/admin , login with admin user

- View http://localhost:8082, to see data of database "django20"

- Add app inside backend named "api" like source code above

- Modify code in /backend/myproject/settings.py and /backend/myproject/urls.py

- Review code with command: docker-compose up

- View http://localhost:8000/api/v1 to see how api working

- View http://localhost:8000/api-token-auth to see how api get token after login into system throught api


# Step 4: build frontend

- Alongside "backend" folder, create "frontend" folder and navigate to inside frontend

- Install NodeJs, Npm, or Yarn

- Use Npm(Yarn) to install "create-react-app" package

- Use "create-react-app" to install the first ReactJs app 

- View http://localhost:3000 to see Reactjs app

# Hope userful for you